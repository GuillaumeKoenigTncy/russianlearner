FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y git && \
    apt-get install -y nano

ADD requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt

ADD . /app
