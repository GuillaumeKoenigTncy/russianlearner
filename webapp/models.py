from django.db import models


class IntegerRangeField(models.IntegerField):
    """ """
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        """ """
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        """ """
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)

class Paires(models.Model):
    """Model definition for MODELNAME."""

    fr_word = models.CharField(max_length=150, null=False, blank=False)
    en_word = models.CharField(max_length=150, null=True, blank=True)
    ru_word = models.CharField(max_length=150, null=False, blank=False)
    ru_gender = models.CharField(max_length=1, null=False, blank=False)
    hint = models.CharField(max_length=250, null=True, blank=True)
    word_note = IntegerRangeField(null=False, blank=False, default=1, min_value=1, max_value=5)
    last_time_learned = models.DateTimeField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=False, blank=False)

    def save(self, *args, **kwargs):
        self.ru_word = self.ru_word.lower()
        self.fr_word = self.fr_word.lower()
        if self.en_word is not None:
            self.en_word = self.en_word.lower()
        return super(Paires, self).save(*args, **kwargs)

    class Meta:
        """Meta definition for MODELNAME."""

        verbose_name = 'Paire'
        verbose_name_plural = 'Paires'

    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.fr_word
