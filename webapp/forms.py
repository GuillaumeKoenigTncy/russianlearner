from django import forms

class AnswerForm(forms.Form):
    answer = forms.CharField(label='Réponse', max_length=100, required=False)
    questiontype = forms.CharField(widget = forms.HiddenInput(), required = False)
    requestedid = forms.CharField(widget = forms.HiddenInput(), required = False)
    wordtoguess = forms.CharField(widget = forms.HiddenInput(), required = False)
    questiontype  = forms.CharField(widget = forms.HiddenInput(), required = False)