import random
from datetime import timedelta
from django.db.models import Q
from django.db.models import F

from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from .models import Paires
from .forms import AnswerForm

from Levenshtein import distance as levenshtein_distance
    

def index(request):

    return render(request, 'index2.html')

    previousquestiontype = None
    previousrequestedid = None
    previouswordtoguess = None
    previousanswer = None
    paire = None
    wasitright = None
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():

            previousquestiontype = form.cleaned_data['questiontype']
            previouswordtoguess = form.cleaned_data['wordtoguess']
            previousrequestedid = form.cleaned_data['requestedid']
            previousanswer = form.cleaned_data['answer'].lower().strip()
            
            paire = Paires.objects.get(id=previousrequestedid)

            if previousquestiontype == "0" or previousquestiontype == "1":

                results_fr = paire.fr_word.split("/")
                results_ru = paire.ru_word.split("/")

                # Deviner mot RUSSE
                # if previouswordtoguess in results_fr and previousanswer in results_ru:
                if previouswordtoguess in results_fr:
                    min_distance = 100
                    for word in results_ru:
                        min_distance = min(min_distance, levenshtein_distance(word.replace(",", "").replace(".", "").replace("!", "").replace("?", "").strip(), previousanswer.replace(",", "").replace(".", "").replace("!", "").replace("?", "").strip()))
                    if min_distance < 1:
                        wasitright = 1
                        if (random.random() <= 0.90 and paire.word_note == 1) or (random.random() <= 0.95 and paire.word_note == 2) or (random.random() <= 0.99 and paire.word_note == 3) or paire.word_note >= 4:
                            Paires.objects.filter(id=previousrequestedid).update(word_note=F('word_note') + 1)
                            Paires.objects.filter(id=previousrequestedid).update(last_time_learned=timezone.now())
                    elif min_distance == 1:
                        wasitright = 0
                    else:
                        wasitright = -1

                # Deviner mot FRANCAIS
                # elif previouswordtoguess in results_ru and previousanswer in results_fr:
                elif previouswordtoguess in results_ru:
                    min_distance = 100
                    for word in results_fr:
                        min_distance = min(min_distance, levenshtein_distance(word.replace(",", "").replace(".", "").replace("!", "").replace("?", "").strip(), previousanswer.replace(",", "").replace(".", "").replace("!", "").replace("?", "").strip()))
                    if min_distance < 1:
                        wasitright = 1
                        if (random.random() <= 0.90 and paire.word_note == 1) or (random.random() <= 0.95 and paire.word_note == 2) or (random.random() <= 0.99 and paire.word_note == 3) or paire.word_note >= 4:
                            Paires.objects.filter(id=previousrequestedid).update(word_note=F('word_note') + 1)
                            Paires.objects.filter(id=previousrequestedid).update(last_time_learned=timezone.now())
                    elif min_distance == 1:
                        wasitright = 0
                    else:
                        wasitright = -1
            elif previousquestiontype == "2":
                if previousanswer.upper() == paire.ru_gender:
                    wasitright = 1
                else:
                    wasitright = -1

        form = AnswerForm()
    else:
        form = AnswerForm()

    paires = Paires.objects.all()
    now = timezone.now()

    possibilities = [0, 1, 0, 1, 0, 1, 2]
    if previousquestiontype is not None:
        possibilities.remove(int(previousquestiontype))

    questiontype = random.choice(possibilities)

    l1 = paires.filter(word_note=1).filter(Q(last_time_learned__lt=now - timedelta(days=1)) | Q(last_time_learned=None))
    l2 = paires.filter(word_note=2).filter(Q(last_time_learned__lt=now - timedelta(days=2)) | Q(last_time_learned=None))
    l3 = paires.filter(word_note=3).filter(Q(last_time_learned__lt=now - timedelta(days=3)) | Q(last_time_learned=None))
    l4 = paires.filter(word_note=4).filter(Q(last_time_learned__lt=now - timedelta(days=5)) | Q(last_time_learned=None))
    l5 = paires.filter(word_note=5).filter(Q(last_time_learned__lt=now - timedelta(days=10)) | Q(last_time_learned=None))

    if questiontype == 2:
        l1 = l1.filter(Q(ru_gender="M") | Q(ru_gender="F") | Q(ru_gender="N"))
        l2 = l2.filter(Q(ru_gender="M") | Q(ru_gender="F") | Q(ru_gender="N"))
        l3 = l3.filter(Q(ru_gender="M") | Q(ru_gender="F") | Q(ru_gender="N"))
        l4 = l4.filter(Q(ru_gender="M") | Q(ru_gender="F") | Q(ru_gender="N"))
        l5 = l5.filter(Q(ru_gender="M") | Q(ru_gender="F") | Q(ru_gender="N"))

    lf = l1.union(l2, l3, l4, l5)
    en_word = None
    if len(lf) != 0:
        choice = random.choices(lf)
        requestedid = choice[0].id
        hint = None

        if questiontype != 2 and (random.random() >= 0.5 or questiontype == 1):
            hint = choice[0].hint
            if choice[0].en_word is not None:
                en_word = choice[0].en_word
            if "/" in choice[0].fr_word:
                choice = choice[0].fr_word.split("/")
                wordtoguess = random.choices(choice)[0]
            else:
                wordtoguess = choice[0].fr_word
        else:
            if "/" in choice[0].ru_word:
                choice = choice[0].ru_word.split("/")
                wordtoguess = random.choices(choice)[0]
            else:
                wordtoguess = choice[0].ru_word
    else:
        context = {"stop": True}
        return render(request, 'index.html', context)

    context = {"paires" : paires, 'form': form, 'requestedid': requestedid, 'wordtoguess': wordtoguess, 'en_word': en_word, 'hint': hint, 'previousrequestedid': previousrequestedid, 'previouswordtoguess': previouswordtoguess, 'previousanswer': previousanswer, 'wasitright': wasitright, 'questiontype': questiontype, "stop": False, 'good' : paire}
    return render(request, 'index.html', context)