from django.contrib import admin
from webapp.models import Paires

# Register your models here.
@admin.register(Paires)
class PairesAdmin(admin.ModelAdmin):
    '''Admin View for Paires'''

    list_display = ('id', 'fr_word', 'en_word', 'ru_word', 'hint', 'word_note', 'created', 'last_time_learned')
    search_fields = ('fr_word', 'en_word', 'ru_word', 'word_note',)